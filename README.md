# Proof of concept for task changing while the dataflow is running.

## Example

### Rabbitmq
docker run -d --hostname my-rabbit --name some-rabbit -p 5672:5672 -p 15672:15672 rabbitmq:3-management

docker network connect bridge some-rabbit

### Producer
docker build -t producer-test ./producer/

docker run -d --net=host --name producer producer-test

### Consumer
docker build -t consumer-test ./consumer/

docker run -d --net=host --name consumer consumer-test